package utils.assertions;

import static org.assertj.core.api.Assertions.assertThat;

public class BaseTest {
    protected void assertResults(final Object actualResult, final Object expectedResult) {
        assertThat(actualResult)
                .withFailMessage("Results are different.")
                .as("Checks results: expected %s and actual %s", expectedResult, actualResult)
                .isEqualTo(expectedResult);
    }
}
