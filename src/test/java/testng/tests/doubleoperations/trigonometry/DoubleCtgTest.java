package testng.tests.doubleoperations.trigonometry;

import org.testng.annotations.Test;
import testng.tests.BaseCalTest;

import java.math.BigDecimal;

import static java.math.MathContext.UNLIMITED;


public class DoubleCtgTest extends BaseCalTest {
    @Test(groups = {"double", "trigonometry"})
    public void avgTest() {
        BigDecimal doubleNumber = new BigDecimal(2.0);
        BigDecimal actualResult = new BigDecimal(calculator.ctg(doubleNumber.doubleValue()), UNLIMITED);
        BigDecimal expectedResult = new BigDecimal(
                Math.cos(doubleNumber.doubleValue())
                        / Math.sin(doubleNumber.doubleValue()),
                UNLIMITED);
        assertResults(actualResult, expectedResult);
    }
}
