package testng.tests.doubleoperations.trigonometry;

import org.testng.annotations.Test;
import testng.tests.BaseCalTest;

import java.math.BigDecimal;

import static java.math.MathContext.UNLIMITED;


public class DoubleSinTest extends BaseCalTest {
    @Test(groups = {"double", "trigonometry"})
    public void avgTest() {
        BigDecimal doubleNumber = new BigDecimal(2.0);
        BigDecimal actualResult = new BigDecimal(calculator.sin(doubleNumber.doubleValue()), UNLIMITED);
        BigDecimal expectedResult = new BigDecimal(Math.sin(doubleNumber.doubleValue()), UNLIMITED);
        assertResults(actualResult, expectedResult);
    }
}
