package testng.tests.doubleoperations;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import testng.tests.BaseCalTest;

import java.math.BigDecimal;

import static java.math.MathContext.UNLIMITED;


public class DoublePovTest extends BaseCalTest {
    @DataProvider(name = "pov", parallel = true)
    public Object[][] getData() {
        return new Object[][]
                {
                        {2.0, 3},
                        {3.0, 10}
                };
    }

    @Parameters({"first", "second"})
    @Test(groups = "double", dataProvider = "pov")
    public void avgTest(final @Optional(value = "2.0") double first, final @Optional(value = "2") int second) {
        BigDecimal doubleNumber = new BigDecimal(first);
        BigDecimal intNumber = new BigDecimal(second);
        BigDecimal actualResult = new BigDecimal(calculator.pow(doubleNumber.doubleValue(), intNumber.doubleValue()), UNLIMITED);
        BigDecimal expectedResult = doubleNumber.pow(intNumber.intValue(), UNLIMITED);
        assertResults(actualResult, expectedResult);
    }
}
