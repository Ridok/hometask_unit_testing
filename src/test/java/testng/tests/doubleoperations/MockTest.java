package testng.tests.doubleoperations;

import com.epam.tat.module4.Calculator;
import org.mockito.Mockito;
import org.testng.annotations.Test;
import utils.assertions.BaseTest;

import static org.mockito.Mockito.when;

public class MockTest extends BaseTest {
    @Test
    public void sumTest() {
        Calculator calculator = Mockito.mock(Calculator.class);
        when(calculator.sum(1.0, 1.0)).thenReturn(new Calculator().sum(1.0, 1.0));
        assertResults(2.0, new Calculator().sum(1.0, 1.0));
    }
}
