package testng.tests.doubleoperations.base;

import org.testng.annotations.Test;
import testng.tests.BaseCalTest;

import java.math.BigDecimal;


public class DoubleSubTest extends BaseCalTest {
    @Test(groups = {"double", "base"})
    public void minTest() {
        BigDecimal minDouble = new BigDecimal(Double.MIN_VALUE);
        BigDecimal actualResult = new BigDecimal(calculator.sub(minDouble.doubleValue(), minDouble.doubleValue()));
        BigDecimal expectedResult = minDouble.subtract(minDouble);
        assertResults(actualResult, expectedResult);
    }

    @Test(groups = {"double", "base"})
    public void maxTest() {
        BigDecimal maxDouble = new BigDecimal(Double.MAX_VALUE);
        BigDecimal actualResult = new BigDecimal(calculator.sub(maxDouble.doubleValue(), maxDouble.doubleValue()));
        BigDecimal expectedResult = maxDouble.subtract(maxDouble);
        assertResults(actualResult, expectedResult);
    }

    @Test(groups = {"double", "base"})
    public void avgTest() {
        BigDecimal doubleNumber = new BigDecimal(1.0);
        BigDecimal actualResult = new BigDecimal(calculator.sub(doubleNumber.doubleValue(), doubleNumber.doubleValue()));
        BigDecimal expectedResult = doubleNumber.subtract(doubleNumber);
        assertResults(actualResult, expectedResult);
    }
}
