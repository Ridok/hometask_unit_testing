package testng.tests.doubleoperations.base;


import org.testng.annotations.Test;
import testng.tests.BaseCalTest;

import java.math.BigDecimal;
import java.util.Random;

import static java.math.MathContext.UNLIMITED;


public class DoubleDivTest extends BaseCalTest {
    @Test(groups = {"double", "base"})
    public void zeroTest() {
        BigDecimal doubleNumber = new BigDecimal(new Random().nextDouble());
        BigDecimal actualResult = new BigDecimal(calculator.div(doubleNumber.doubleValue(), 0.0), UNLIMITED);
        BigDecimal expectedResult = doubleNumber.divide(new BigDecimal(0.0), UNLIMITED);
        assertResults(actualResult, expectedResult);
    }

    @Test(groups = {"double", "base"})
    public void avgTest() {
        BigDecimal doubleNumber = new BigDecimal(1.0);
        BigDecimal actualResult = new BigDecimal(calculator.div(doubleNumber.doubleValue(), doubleNumber.doubleValue()), UNLIMITED);
        BigDecimal expectedResult = doubleNumber.divide(doubleNumber, UNLIMITED);
        assertResults(actualResult, expectedResult);
    }
}
