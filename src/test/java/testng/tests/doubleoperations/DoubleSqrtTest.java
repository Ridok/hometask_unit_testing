package testng.tests.doubleoperations;

import org.testng.annotations.Test;
import testng.tests.BaseCalTest;


public class DoubleSqrtTest extends BaseCalTest {
    @Test(groups = "double")
    public void avgTest() {
        assertResults(3.0, calculator.sqrt(9.0));
    }

    @Test(groups = "double")
    public void zeroTest() {
        assertResults(0.0, calculator.sqrt(0.0));
    }
}
