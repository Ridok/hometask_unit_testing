package testng.tests;

import com.epam.tat.module4.Calculator;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import utils.assertions.BaseTest;

public class BaseCalTest extends BaseTest {
    protected Calculator calculator;

    @BeforeMethod(alwaysRun = true)
    public void setUpCalculator() {
        calculator = new Calculator();
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(){
        calculator = null;
    }
}
