package testng.tests.longoperations;

import org.testng.annotations.Test;
import testng.tests.BaseCalTest;

import java.math.BigDecimal;


public class LongIsPositiveTest extends BaseCalTest {
    @Test
    public void isPositiveTest() {
        BigDecimal longNumber = new BigDecimal(1L);
        assertResults(calculator.isPositive(longNumber.longValue()), true);
    }

    @Test
    public void isPositiveWithZeroTest() {
        BigDecimal longNumber = new BigDecimal(0L);
        assertResults(calculator.isPositive(longNumber.longValue()), false);
    }

    @Test
    public void isPositiveWithNegativeTest() {
        BigDecimal longNumber = new BigDecimal(-1L);
        assertResults(calculator.isPositive(longNumber.longValue()), false);
    }
}
