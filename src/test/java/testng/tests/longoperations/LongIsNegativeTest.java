package testng.tests.longoperations;

import org.testng.annotations.Test;
import testng.tests.BaseCalTest;

import java.math.BigDecimal;


public class LongIsNegativeTest extends BaseCalTest {
    @Test
    public void isNegativeTest() {
        BigDecimal longNumber = new BigDecimal(-1L);
        assertResults(calculator.isNegative(longNumber.longValue()), true);
    }

    @Test
    public void isNegativeWithZeroTest() {
        BigDecimal longNumber = new BigDecimal(0L);
        assertResults(calculator.isNegative(longNumber.longValue()), false);
    }

    @Test
    public void isNegativeWithPositiveTest() {
        BigDecimal longNumber = new BigDecimal(1L);
        assertResults(calculator.isNegative(longNumber.longValue()), false);
    }
}
