package testng.tests.longoperations.base;

import org.testng.annotations.Test;
import testng.tests.BaseCalTest;

import java.math.BigDecimal;
import java.util.Random;

import static java.math.MathContext.UNLIMITED;


public class LongDivTest extends BaseCalTest {
    @Test
    public void zeroTest() {
        BigDecimal longNumber = new BigDecimal(new Random().nextLong());
        BigDecimal actualResult = new BigDecimal(calculator.div(longNumber.longValue(), 0L), UNLIMITED);
        BigDecimal expectedResult = longNumber.divide(new BigDecimal(0L), UNLIMITED);
        assertResults(actualResult, expectedResult);
    }

    @Test
    public void avgTest() {
        BigDecimal longNumber = new BigDecimal(1L);
        BigDecimal actualResult = new BigDecimal(calculator.div(longNumber.longValue(), longNumber.longValue()), UNLIMITED);
        BigDecimal expectedResult = longNumber.divide(longNumber, UNLIMITED);
        assertResults(actualResult, expectedResult);
    }
}
