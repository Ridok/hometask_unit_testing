package testng.tests.longoperations.base;

import org.testng.annotations.Test;
import testng.tests.BaseCalTest;

import java.math.BigDecimal;


public class LongSubTest extends BaseCalTest {
    @Test
    public void minTest() {
        BigDecimal minLong = new BigDecimal(Long.MIN_VALUE);
        BigDecimal actualResult = new BigDecimal(calculator.sub(minLong.longValue(), minLong.longValue()));
        BigDecimal expectedResult = minLong.subtract(minLong);
        assertResults(actualResult, expectedResult);
    }

    @Test
    public void maxTest() {
        BigDecimal maxLong = new BigDecimal(Long.MAX_VALUE);
        BigDecimal actualResult = new BigDecimal(calculator.sub(maxLong.longValue(), maxLong.longValue()));
        BigDecimal expectedResult = maxLong.subtract(maxLong);
        assertResults(actualResult, expectedResult);
    }

    @Test
    public void avgTest() {
        BigDecimal longNumber = new BigDecimal(1L);
        BigDecimal actualResult = new BigDecimal(calculator.sub(longNumber.longValue(), longNumber.longValue()));
        BigDecimal expectedResult = longNumber.subtract(longNumber);
        assertResults(actualResult, expectedResult);
    }
}
