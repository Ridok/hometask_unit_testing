package junit5.tests.doubleoperations.base;

import junit5.tests.BaseCalTest;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;


public class DoubleMultTest extends BaseCalTest {
    @Test
    public void minTest() {
        BigDecimal minDouble = new BigDecimal(Double.MIN_VALUE);
        BigDecimal actualResult = new BigDecimal(calculator.mult(minDouble.doubleValue(), minDouble.doubleValue()));
        BigDecimal expectedResult = minDouble.multiply(minDouble);
        assertResults(actualResult, expectedResult);
    }

    @Test
    public void maxTest() {
        BigDecimal maxDouble = new BigDecimal(Double.MAX_VALUE);
        BigDecimal actualResult = new BigDecimal(calculator.mult(maxDouble.doubleValue(), maxDouble.doubleValue()));
        BigDecimal expectedResult = maxDouble.multiply(maxDouble);
        assertResults(actualResult, expectedResult);
    }

    @Test
    public void avgTest() {
        BigDecimal doubleNumber = new BigDecimal(1.0);
        BigDecimal actualResult = new BigDecimal(calculator.mult(doubleNumber.doubleValue(), doubleNumber.doubleValue()));
        BigDecimal expectedResult = doubleNumber.multiply(doubleNumber);
        assertResults(actualResult, expectedResult);
    }
}
