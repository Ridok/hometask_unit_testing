package junit5.tests.doubleoperations;

import junit5.tests.BaseCalTest;
import org.junit.jupiter.api.Test;


public class DoubleSqrtTest extends BaseCalTest {
    @Test
    public void avgTest() {
        assertResults(3.0, calculator.sqrt(9.0));
    }

    @Test
    public void zeroTest() {
        assertResults(0.0, calculator.sqrt(0.0));
    }
}
