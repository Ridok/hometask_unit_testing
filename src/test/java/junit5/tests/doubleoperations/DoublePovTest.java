package junit5.tests.doubleoperations;

import junit5.tests.BaseCalTest;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static java.math.MathContext.UNLIMITED;


public class DoublePovTest extends BaseCalTest {
    @Test
    public void avgTest() {
        BigDecimal doubleNumber = new BigDecimal(2.0);
        BigDecimal intNumber = new BigDecimal(2);
        BigDecimal actualResult = new BigDecimal(calculator.pow(doubleNumber.doubleValue(), intNumber.doubleValue()), UNLIMITED);
        BigDecimal expectedResult = doubleNumber.pow(intNumber.intValue(), UNLIMITED);
        assertResults(actualResult, expectedResult);
    }
}
