package junit5.tests.doubleoperations.trigonometry;

import junit5.tests.BaseCalTest;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static java.math.MathContext.UNLIMITED;


public class DoubleCosTest extends BaseCalTest {
    @Test
    public void avgTest() {
        BigDecimal doubleNumber = new BigDecimal(2.0);
        BigDecimal actualResult = new BigDecimal(calculator.cos(doubleNumber.doubleValue()), UNLIMITED);
        BigDecimal expectedResult = new BigDecimal(Math.cos(doubleNumber.doubleValue()), UNLIMITED);
        assertResults(actualResult, expectedResult);
    }
}
