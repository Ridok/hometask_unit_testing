package junit5.tests.doubleoperations.trigonometry;

import junit5.tests.BaseCalTest;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static java.math.MathContext.UNLIMITED;


public class DoubleSinTest extends BaseCalTest {
    @Test
    public void avgTest() {
        BigDecimal doubleNumber = new BigDecimal(2.0);
        BigDecimal actualResult = new BigDecimal(calculator.sin(doubleNumber.doubleValue()), UNLIMITED);
        BigDecimal expectedResult = new BigDecimal(Math.sin(doubleNumber.doubleValue()), UNLIMITED);
        assertResults(actualResult, expectedResult);
    }
}
