package junit5.tests;

import com.epam.tat.module4.Calculator;
import junit5.utils.LoggingExtension;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import utils.assertions.BaseTest;

@ExtendWith(LoggingExtension.class)
public class BaseCalTest extends BaseTest {
    protected Calculator calculator;
    protected Logger logger;

    @BeforeEach
    public void setUpCalculator() {
        calculator = new Calculator();
        logger.info("Calculator was started.");

    }

    @AfterEach()
    public void tearDown(){
        calculator = null;
        logger.info("Calculator was finished.");
    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }
}
