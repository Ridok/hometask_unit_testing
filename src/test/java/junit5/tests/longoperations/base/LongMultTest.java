package junit5.tests.longoperations.base;

import junit5.tests.BaseCalTest;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;


public class LongMultTest extends BaseCalTest {
    @Test
    public void minTest() {
        BigDecimal minLong = new BigDecimal(Long.MIN_VALUE);
        BigDecimal actualResult = new BigDecimal(calculator.mult(minLong.longValue(), minLong.longValue()));
        BigDecimal expectedResult = minLong.multiply(minLong);
        assertResults(actualResult, expectedResult);
    }

    @Test
    public void maxTest() {
        BigDecimal maxLong = new BigDecimal(Long.MAX_VALUE);
        BigDecimal actualResult = new BigDecimal(calculator.mult(maxLong.longValue(), maxLong.longValue()));
        BigDecimal expectedResult = maxLong.multiply(maxLong);
        assertResults(actualResult, expectedResult);
    }

    @Test
    public void avgTest() {
        BigDecimal longNumber = new BigDecimal(1L);
        BigDecimal actualResult = new BigDecimal(calculator.mult(longNumber.longValue(), longNumber.longValue()));
        BigDecimal expectedResult = longNumber.multiply(longNumber);
        assertResults(actualResult, expectedResult);
    }
}
