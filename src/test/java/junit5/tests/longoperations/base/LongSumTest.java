package junit5.tests.longoperations.base;

import junit5.tests.BaseCalTest;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;


public class LongSumTest extends BaseCalTest {
    @Test
    public void minTest() {
        BigDecimal minLong = new BigDecimal(Long.MIN_VALUE);
        BigDecimal actualResult = new BigDecimal(calculator.sum(minLong.longValue(), minLong.longValue()));
        BigDecimal expectedResult = minLong.add(minLong);
        assertResults(actualResult, expectedResult);
    }

    @Test
    public void maxTest() {
        BigDecimal maxLong = new BigDecimal(Long.MAX_VALUE);
        BigDecimal actualResult = new BigDecimal(calculator.sum(maxLong.longValue(), maxLong.longValue()));
        BigDecimal expectedResult = maxLong.add(maxLong);
        assertResults(actualResult, expectedResult);
    }

    @Test
    public void avgTest() {
        BigDecimal longNumber = new BigDecimal(1L);
        BigDecimal actualResult = new BigDecimal(calculator.sum(longNumber.longValue(), longNumber.longValue()));
        BigDecimal expectedResult = longNumber.add(longNumber);
        assertResults(actualResult, expectedResult);
    }
}
