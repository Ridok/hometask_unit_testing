package junit5.tests.longoperations;

import junit5.tests.BaseCalTest;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;


public class LongIsPositiveTest extends BaseCalTest {
    @Test
    public void isPositiveTest() {
        BigDecimal longNumber = new BigDecimal(1L);
        assertResults(calculator.isPositive(longNumber.longValue()), true);
    }

    @Test
    public void isPositiveWithZeroTest() {
        BigDecimal longNumber = new BigDecimal(0L);
        assertResults(calculator.isPositive(longNumber.longValue()), false);
    }

    @Test
    public void isPositiveWithNegativeTest() {
        BigDecimal longNumber = new BigDecimal(-1L);
        assertResults(calculator.isPositive(longNumber.longValue()), false);
    }
}
